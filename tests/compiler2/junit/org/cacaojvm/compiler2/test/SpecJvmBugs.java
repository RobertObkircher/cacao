/** tests/compiler2/junit/SpecJvmBugs.java - some bugs that were triggered by SpecJVM
 *
 * Copyright (C) 1996-2024
 * CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO
 *
 * This file is part of CACAO.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */
package org.cacaojvm.compiler2.test;

import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;

public class SpecJvmBugs extends Compiler2TestBase {

	/**
	* The reduced version of testToStringOpenJDK below.
	*/
	@Test
	public void testInfiniteLoop() {
		testResultEqual("infiniteLoop", "(Z)I", true);
	}

	public static int infiniteLoop(boolean b) {
		int x = 0;
		for (;;) {
			x += b ? 1 : 2;
			if (x == 42)
				return x;
		}
	}

	/**
	* This triggered the following assertion:
	* cacao: SSAConstructionPass.cpp:83: void cacao::jit::compiler2::InVarPhis::fill_operands(): Assertion `parent->sealed_blocks[pred_nr]' failed.
	*
	* It turned out that the assertion was invalid because phi operands can in fact be from a
	* predecessor that hasn't been sealed yet. The only requirement is that is was filled.
	*/
	@Test
	public void testToStringOpenJDK() {
		AbstractCollection<String> list = new ArrayList<String>() {
			@Override
			public String toString() {
				Iterator it = iterator();
				if (! it.hasNext())
					return "[]";

				StringBuilder sb = new StringBuilder();
				sb.append('[');
				for (;;) {
					Object e = it.next();
					sb.append(e == this ? "(this Collection)" : e);
					if (! it.hasNext())
						return sb.append(']').toString();
					sb.append(',').append(' ');
				}
			}
		};
		list.add("a");
		list.add("b");
		list.add("c");
		testResultEqual(list.getClass(), "toString", "()Ljava/lang/String;", list);
	}

	/**
	* This creates a Phi instruction which merges two constant strings.
	* That caused has_only_constant_operands in ConstantPropagationPass to throw
	* a runtime_error because it didn't handle references.
	* This was found while reducing testToStringOpenJDK.
	*/
	@Test
	public void testReferenceConstants() {
		testResultEqual("referenceConstants", "(Z)Ljava/lang/String;", true);
		testResultEqual("referenceConstants2", "(Z)Ljava/lang/String;", true);
	}

	public static String referenceConstants(boolean b) {
		return b ? "x" : "y";
	}

	public static String referenceConstants2(boolean b) {
		return b ? "x" : "x";
	}

	/**
	* This is a heavily reduced version of com.sun.org.apache.xerces.internal.util.URI.isWellFormedAddress.
	* When it was compiled with compiler2 the i variable in the loop would get large negative numbers.
	*
	* The problem was an incorrect dominator computation. It follows an algorithm from
	* a paper but in the Eval function the call to Compress was missing.
	*
	* Because i+1 and i++ got combined by global value numbering late scheduling placed the computation
	* within if (testChar == '.') because the dominator wasn't the block before the if.
	*/
	@Test
	public void testDominatorBugDirect() throws Throwable {
		Object result = runCompiler2("isWellFormedAddress", "(C)I", 'x');
		assertEquals(result, 11);
	}

	@Test
	public void testDominatorBug() {
		testResultEqual("isWellFormedAddress", "(C)I", 'x');
	}

	public static int isWellFormedAddress(char testChar) {
		int i = 0;
		for (; i < 11; i++) {
			//System.out.println(i);
			if (testChar == '.') {
				if (i+1 < 5 ) { // global value numbering eliminated i++ in favor of this one
					return 0;
				}
				testChar = 0;
			}
			else if (testChar == '3') {
				return 0;
			}
			else if (testChar == '4') {
				return 0;
			}
		}
		return i;
	}

	/**
	* It is a simplified version of testDiv in src/spec/benchmarks/check/PepTest.java
	*
	* Initially it crashed compiler2 while propagating a constant division by zero.
	* But this example also fails because the division gets dead-code-eliminated which
	* causes it to return failed5.
	*
	* TODO ensure that exceptions from divisions don't get eliminated or moved around
	*/
	@Ignore
	@Test
	public void testDivFromPepTest() {
		testResultEqual("testDiv", "()Ljava/lang/String;");
	}

	public static String testDiv() {
		int a = 1;
		int b = 0;
		try {
			a = a / b;
			return "failed 5";
		} catch (java.lang.Exception x) {
			return null;
		}
	}

	/**
	* The seedUniquifier of java/util/Random aborted compilation in read_variable_recursive with an exception.
	* The problem was that the extra init bb that was generated because of the loop didn't use the javalocals
	* from the baseline compiler and thus tried to read variables that weren't live.
	*/
	@Test
	public void testRandomSeedUniquifier() {
		testResultEqual("seedUniquifier", "()J");
	}

    private static long seedUniquifier = 42;

    private static long seedUniquifier() {
        for (;;) {
            long next = seedUniquifier;
            if ((seedUniquifier = next) == next)
                return next;
        }
    }

    /**
    * When running with inlining it crashed in BasicBlockSchedulingPass because
    * an unconditional deoptimization in an inlined method resulted in an extra
    * predecessor where the dead branch merged with the life one.
    */
    @Test
    public void testInliningBug() throws Exception {
		testResultEqual("inlineOuter", "(Z)I", true);
		testResultEqual("inlineOuter", "(Z)I", false);
    }

    public static void inlineInner() {
        Compiler2TestBase.deoptimizeHere();
    }

    public static int inlineOuter(boolean b) {
        int i = 0;
        if (b) {
            i += 5;
            inlineInner(); // will deopt when inlined
            // there could be a loop here, so recursively deleting is difficult
        }
        return i; // need to remove dead predecessor and phi operands
    }

}
