/** tests/compiler2/junit/Deoptimize.java
*
* Copyright (C) 1996-2023
* CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO
*
* This file is part of CACAO.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301, USA.
*
*/
package org.cacaojvm.compiler2.test;

import org.junit.Ignore;
import org.junit.Test;

public class Deoptimize extends Compiler2TestBase {

	@Test
	public void test1() {
		testResultEqual("deoptimizeByte", "(B)B", (byte) 42);
	}

	static byte deoptimizeByte(byte b) {
		Compiler2Test.deoptimizeHere();
		return b;
	}

	@Ignore("Deoptimization with boolean, byte, short and char values is not supported yet.")
	@Test
	public void test2() {
		testResultEqual("deoptimizeByte", "(J)B", 1027L);
	}

	static byte deoptimizeByte(long x) {
		byte b = (byte) x;
		Compiler2Test.deoptimizeHere();
		return b;
	}

	@Test
	public void test3() {
		testResultEqual("deoptimizeMany", "(BIJBIJBIJBIJ)Ljava/lang/String;",
			(byte) 1, 100000, 10000000000L,
			(byte) 2, 200000, 20000000000L,
			(byte) 3, 300000, 30000000000L,
			(byte) 4, 400000, 40000000000L
		);
	}

	static String deoptimizeMany(
		byte b1, int i1, long j1,
		byte b2, int i2, long j2,
		byte b3, int i3, long j3,
		byte b4, int i4, long j4
	) {
		// spilling creates ManagedStackSlots
		int x1 = i1 + 1;
		int x2 = i2 + 1;
		int x3 = i3 + 1;
		int x4 = i4 + 1;
		long y1 = j1 + 1;
		long y2 = j2 + 1;
		long y3 = j3 + 1;
		long y4 = j4 + 1;
		Compiler2Test.deoptimizeHere();
		return "b1=" + b1 + ", i1=" + i1 + ", j1=" + j1 + ", x1=" + x1 + ", y1=" + y1 + "\n" +
		       "b2=" + b2 + ", i2=" + i2 + ", j2=" + j2 + ", x2=" + x2 + ", y2=" + y2 + "\n" +
		       "b3=" + b3 + ", i3=" + i3 + ", j3=" + j3 + ", x3=" + x3 + ", y3=" + y3 + "\n" +
		       "b4=" + b4 + ", i4=" + i4 + ", j4=" + j4 + ", x4=" + x4 + ", y4=" + y4;
	}

}