/* src/vm/jit/x86_64/linux/md-os.cpp - machine dependent x86_64 Linux functions

   Copyright (C) 1996-2013
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/


#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include "config.h"

#include <cassert>
#include <cstdlib>
#include <stdint.h>
#include <ucontext.h>

#include "vm/types.hpp"

#include "vm/jit/x86_64/codegen.hpp"
#include "vm/jit/x86_64/md.hpp"

#include "threads/thread.hpp"

#include "vm/signallocal.hpp"

#include "vm/jit/executionstate.hpp"
#include "vm/jit/trap.hpp"


/**
 * Signal handler for hardware exception.
 */
void md_signal_handler_sigsegv(int sig, siginfo_t *siginfo, void *_p)
{
	ucontext_t* _uc = (ucontext_t *) _p;
	mcontext_t* _mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	void* xpc = (void*) _mc->gregs[REG_RIP];

	// Handle the trap.
	trap_handle(TRAP_SIGSEGV, xpc, _p);
}


/**
 * ArithmeticException signal handler for hardware divide by zero
 * check.
 */
void md_signal_handler_sigfpe(int sig, siginfo_t *siginfo, void *_p)
{
	ucontext_t* _uc = (ucontext_t *) _p;
	mcontext_t* _mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	void* xpc = (void*) _mc->gregs[REG_RIP];

	// Handle the trap.
	trap_handle(TRAP_SIGFPE, xpc, _p);
}


/**
 * Signal handler for patchers.
 */
void md_signal_handler_sigill(int sig, siginfo_t *siginfo, void *_p)
{
	ucontext_t* _uc = (ucontext_t *) _p;
	mcontext_t* _mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	void* xpc = (void*) _mc->gregs[REG_RIP];

	// Handle the trap.
	trap_handle(TRAP_SIGILL, xpc, _p);
}


/* md_signal_handler_sigusr2 ***************************************************

   Signal handler for profiling sampling.

*******************************************************************************/

void md_signal_handler_sigusr2(int sig, siginfo_t *siginfo, void *_p)
{
	threadobject *t;
	ucontext_t   *_uc;
	mcontext_t   *_mc;
	u1           *pc;

	t = THREADOBJECT;

	_uc = (ucontext_t *) _p;
	_mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	pc = (u1 *) _mc->gregs[REG_RIP];

	t->pc = pc;
}


/* md_executionstate_read ******************************************************

   Read the given context into an executionstate.

*******************************************************************************/

void md_executionstate_read(executionstate_t *es, void *context)
{
	ucontext_t *_uc;
	mcontext_t *_mc;

	_uc = (ucontext_t *) context;
	_mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	/* read special registers */
	es->pc = (u1 *) _mc->gregs[REG_RIP];
	es->sp = (u1 *) _mc->gregs[REG_RSP];
	es->pv = NULL;

	static_assert(REG_RCX == 14, "REG_RCX must be the one from ucontext.h");
	static_assert(RCX == 1, "RAX must be the one from md-abi.hpp");
	es->intregs[RAX] = _mc->gregs[REG_RAX];
	es->intregs[RCX] = _mc->gregs[REG_RCX];
	es->intregs[RDX] = _mc->gregs[REG_RDX];
	es->intregs[RBX] = _mc->gregs[REG_RBX];
	es->intregs[RSP] = _mc->gregs[REG_RSP];
	es->intregs[RBP] = _mc->gregs[REG_RBP];
	es->intregs[RSI] = _mc->gregs[REG_RSI];
	es->intregs[RDI] = _mc->gregs[REG_RDI];
	es->intregs[R8] = _mc->gregs[REG_R8];
	es->intregs[R9] = _mc->gregs[REG_R9];
	es->intregs[R10] = _mc->gregs[REG_R10];
	es->intregs[R11] = _mc->gregs[REG_R11];
	es->intregs[R12] = _mc->gregs[REG_R12];
	es->intregs[R13] = _mc->gregs[REG_R13];
	es->intregs[R14] = _mc->gregs[REG_R14];
	es->intregs[R15] = _mc->gregs[REG_R15];

	/* read float registers */
	static_assert(sizeof (es->fltregs[0]) == sizeof (_mc->fpregs->_xmm[0]), "unexpected float register size");
	static_assert(sizeof (es->fltregs) == sizeof (_mc->fpregs->_xmm), "unexpected float register count");
	os::memcpy(es->fltregs, _mc->fpregs->_xmm, sizeof (es->fltregs));
}


/* md_executionstate_write *****************************************************

   Write the given executionstate back to the context.

*******************************************************************************/

void md_executionstate_write(executionstate_t *es, void *context)
{
	ucontext_t *_uc;
	mcontext_t *_mc;

	_uc = (ucontext_t *) context;
	_mc = &_uc->uc_mcontext;

	/* ATTENTION: Don't use CACAO's internal REG_* defines as they are
	   different to the ones in <ucontext.h>. */

	/* write integer registers */
	static_assert(REG_RCX == 14, "REG_RCX must be the one from ucontext.h");
	static_assert(RCX == 1, "RAX must be the one from md-abi.hpp");
	_mc->gregs[REG_RAX] = (greg_t) es->intregs[RAX];
	_mc->gregs[REG_RCX] = (greg_t) es->intregs[RCX];
	_mc->gregs[REG_RDX] = (greg_t) es->intregs[RDX];
	_mc->gregs[REG_RBX] = (greg_t) es->intregs[RBX];
	_mc->gregs[REG_RSP] = (greg_t) es->intregs[RSP];
	_mc->gregs[REG_RBP] = (greg_t) es->intregs[RBP];
	_mc->gregs[REG_RSI] = (greg_t) es->intregs[RSI];
	_mc->gregs[REG_RDI] = (greg_t) es->intregs[RDI];
	_mc->gregs[REG_R8]  = (greg_t) es->intregs[R8];
	_mc->gregs[REG_R9]  = (greg_t) es->intregs[R9];
	_mc->gregs[REG_R10] = (greg_t) es->intregs[R10];
	_mc->gregs[REG_R11] = (greg_t) es->intregs[R11];
	_mc->gregs[REG_R12] = (greg_t) es->intregs[R12];
	_mc->gregs[REG_R13] = (greg_t) es->intregs[R13];
	_mc->gregs[REG_R14] = (greg_t) es->intregs[R14];
	_mc->gregs[REG_R15] = (greg_t) es->intregs[R15];

	/* write special registers */
	_mc->gregs[REG_RIP] = (greg_t) (ptrint) es->pc;
	_mc->gregs[REG_RSP] = (greg_t) (ptrint) es->sp;

	/* write float registers */
	static_assert(sizeof (es->fltregs[0]) == sizeof (_mc->fpregs->_xmm[0]), "unexpected float register size");
	static_assert(sizeof (es->fltregs) == sizeof (_mc->fpregs->_xmm), "unexpected float register count");
	os::memcpy(_mc->fpregs->_xmm, es->fltregs, sizeof (es->fltregs));
}


/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
