/* src/vm/jit/compiler2/Backend.cpp - Backend

   Copyright (C) 2013
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/

#include "vm/jit/compiler2/Backend.hpp"
#include "vm/jit/compiler2/JITData.hpp"
#include "vm/jit/compiler2/MachineBasicBlock.hpp"
#include "vm/jit/compiler2/MachineInstructions.hpp"
#include "vm/jit/compiler2/MachineOperandFactory.hpp"

# include "vm/jit/replace.hpp"

#include "Target.hpp"

namespace cacao {
namespace jit {
namespace compiler2 {

Backend* Backend::factory(JITData *JD) {
	return new BackendBase<Target>(JD, new RegisterInfoBase<Target>(JD));
}

void LoweringVisitorBase::visit(BeginInst* I, bool copyOperands) {
	assert(I);
	MachineInstruction *label = new MachineLabelInst();
	get_current()->push_back(label);
	//set_op(I,label->get_result().op);
}

void LoweringVisitorBase::visit(GOTOInst* I, bool copyOperands) {
	assert(I);
	MachineInstruction *jump = backend->create_Jump(get(I->get_target().get()));
	get_current()->push_back(jump);
	get_current()->set_last_insertion_point(get_current()->mi_last());
	set_op(I,jump->get_result().op);
}

void LoweringVisitorBase::visit(PHIInst* I, bool copyOperands) {
	assert(I);
	MachinePhiInst *phi = new MachinePhiInst(I->op_size(),I->get_type(),I,backend->get_JITData()->get_MachineOperandFactory());
	phi->set_block(get_current()); // TODO: This shoudl really happen in the MBB
	//get_current()->push_back(phi);
	get_current()->insert_phi(phi);
	set_op(I,phi->get_result().op);
}

void LoweringVisitorBase::visit(CONSTInst* I, bool copyOperands) {
	assert(I);
	// auto MOF = backend->get_JITData()->get_MachineOperandFactory();
	// VirtualRegister *reg = MOF->CreateVirtualRegister(I->get_type());
	Immediate *imm = new Immediate(I);
	// MachineInstruction *move = backend->create_Move(imm,reg);
	// get_current()->push_back(move);
	// set_op(I,move->get_result().op);

	// TODO: We do not mov immediates into registers upfront but let
	//       each instruction decide on their own what to do with immediates.
	//       This should later be reversed again and handled by the DAG matcher.
	set_op(I, imm);
}

void LoweringVisitorBase::lower_source_state_dependencies(MachineReplacementPointInst *MI,
		SourceStateInst *source_state) {
	assert(MI);
	assert(source_state);

	int op_index = 0;

	// TODO: Remove once we decide how to lower constants? This is the same code as X86_64LoweringVisitor::loadImmediate
	auto loadImmediate = [this](MachineOperand *mop)-> MachineOperand* {
		auto MOF = get_Backend()->get_JITData()->get_MachineOperandFactory();
		VirtualRegister* reg = MOF->CreateVirtualRegister(mop->get_type());
		MachineInstruction* move = get_Backend()->create_Move(mop, reg);
		get_current()->push_back(move);
		return reg;
	};

	// TODO deduplicate the machine operands and store an additional
	//      vector of { javalocal_index, machine_op_index }. That would
	//      reduce the number of allocated registers and stack slots.

	for (; source_state != nullptr; source_state = source_state->get_parent()) {
		size_t index = 0;
		for (SourceStateInst::const_rplalloc_index_iterator
		         i = source_state->rplalloc_indices_begin(),
		         e = source_state->rplalloc_indices_end();
		     i != e; ++i, ++index)
		{
			Value *value = source_state->get_operand(index);
			assert(value);
			Instruction *op = value->to_Instruction();
			assert(op);

			MachineOperand* mop = get_op(op);
			if (mop->to_Immediate()) mop = loadImmediate(mop);
			MI->set_operand(op_index, mop);
			MI->set_javalocal_index(op_index, *i);
			op_index++;
		}
	}
}

void LoweringVisitorBase::place_deoptimization_marker(SourceStateAwareInst *I) {
	SourceStateInst *source_state = I->get_source_state();

	if (source_state) {
		MachineDeoptInst *deopt = new MachineDeoptInst(source_state);
		lower_source_state_dependencies(deopt, source_state);
		get_current()->push_back(deopt);
	}
}

void LoweringVisitorBase::visit(SourceStateInst* I, bool copyOperands) {
	// A SouceStateInst is just an artificial instruction for holding metadata
	// for ReplacementPointInsts. It has no direct pendant on LIR level and
	// hence needs no lowering logic.
}

void LoweringVisitorBase::visit(ReplacementEntryInst* I, bool copyOperands) {
	MachineReplacementEntryInst *MI = new MachineReplacementEntryInst(I->get_rpid());
	MI->results_clear();

	get_current()->push_back(MI);

	// restore original order
	auto loads = alloc::vector<LOADInst*>::type();
	loads.reserve(I->user_size());
	for (auto i = I->user_begin(), e = I->user_end(); i != e; ++i) {
		LOADInst* load = (*i)->to_LOADInst();
		if (load == nullptr) {
			os::abort("expected LOADInst");
		}
		loads.push_back(load);
	}
	std::sort(loads.begin(), loads.end(), [](auto a, auto b) {
		return a->get_index() < b->get_index();
	});

	for (const auto& load : loads) {
		// NOTE: The LOADInsts in the MachineMethodDescriptor use MOF->CreateStackSlot
		//       but here we do need to use the StackSlotManager to reserve space on in the stack frame.
		//       I'm not sure if this is a good solution, or if it would be better to pass the values like arguments.
		ManagedStackSlot *src = get_Backend()->get_JITData()->get_StackSlotManager()->create_slot(load->get_type());

		MachineOperand *dst = CreateVirtualRegister(load->get_type());
		get_current()->push_back(get_Backend()->create_Move(src, dst));
		set_op(load, dst);

		MI->push_javalocal_index(I->get_rplalloc_index(load->get_index()));
		MI->add_result(src);
	}
}

MachineBasicBlock* LoweringVisitorBase::new_block() const {
	return *schedule->insert_after(get_current()->self_iterator(),MBBBuilder());
}

VirtualRegister* LoweringVisitorBase::CreateVirtualRegister(Type::TypeID type) {
	return backend->get_JITData()->get_MachineOperandFactory()->CreateVirtualRegister(type);
}

OperandSet RegisterInfo::get_AllCalleeSaved() const {
	auto set = JD->get_Backend()->get_NativeFactory()->EmptySet();
	for (const auto& clazz : classes) {
		set |= clazz->get_CalleeSaved();
	}
	return set;
}


} // end namespace compiler2
} // end namespace jit
} // end namespace cacao


/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
