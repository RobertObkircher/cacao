/* src/vm/jit/compiler2/HIRManipulations.hpp - HIRManipulations

   Copyright (C) 2013
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/
#ifndef _JIT_COMPILER2_HIRMANIPULATIONS
#define _JIT_COMPILER2_HIRMANIPULATIONS

#include "vm/jit/compiler2/Instructions.hpp"

namespace cacao {
namespace jit {
namespace compiler2 {

class HIRManipulations {
private:
    class SplitBasicBlockOperation;
    class CoalesceBasicBlocksOperation;
public:
    /**
     * Splits a basic block at a given instruction. All depending instructions will be moved into the new basic block.
     * Additionally the end instruction of the given basic block will be moved to the new basic block, even if no 
     * dependency exists. The given basic block will not have an EndInst after this method. The instruction itself
     * will still be in the old basic block.
     * @returns the new basic block
     **/
	static BeginInst* split_basic_block(BeginInst* bb, Instruction* split_at);
    /**
     * Moves a given instruction into another method.
     **/
	static void move_instruction_to_method(Instruction* to_move, Method* target_method);
    /**
     * Connects two basic blocks with an GOTOInst.
     **/
	static void connect_with_jump(BeginInst* source, BeginInst* target);
    /**
     * Removes an instruction including all dependency relationships. Additionally the instruction
     * will be removed from the containing method and deleted.
     *
     * Keep in mind that this (currently) doesn't create edges from reverse dependencies to dependencies!
     */
	static void remove_instruction(Instruction* to_remove);
    /**
     * Coalesce basic blocks.
     */
	static void coalesce_basic_blocks(Method* M);
    /**
     * Replaces a value with another one in every user except source states.
     */
	static void force_set_beginInst(Instruction* I, BeginInst* new_bb);

	typedef alloc::unordered_set<Instruction*>::type InstSetTy;

	/**
	 * Collects all instructions that recursively depend on I.
	 *
	 * @param append_to The result.
	 * @param I The instruction where to start.
	 * @param bb The basic block that contains the instruction.
	 */
	static void append_local_scheduling_graph(InstSetTy *append_to, Instruction *I, BeginInst *bb);

	/**
	 * Remove the predecessor at the given index from succ and its phi nodes.
	 *
	 * Does *not* update the successor of the predecessors end instruction.
	 */
	static void remove_bb_pred(BeginInst *succ, int index);

	/**
	 * Remove predecessor and successor edges from this basic block and update phi nodes.
	 */
	static void disconnect_bb(BeginInst *begin);

	/**
	 * Traverses all blocks reachable from the initial basic block via successor edges and
	 * returns the blocks that are unreachable.
	 */
	static alloc::vector<BeginInst*>::type collect_dead_predecessors(Method *method);
};

} // end namespace compiler2
} // end namespace jit
} // end namespace cacao

#undef DEBUG_NAME

#endif /* _JIT_COMPILER2_HIRMANIPULATIONS */

/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
