/* src/vm/jit/compiler2/MachineInstructions.hpp - Machine Instruction Types

   Copyright (C) 2013
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/

#ifndef _JIT_COMPILER2_MACHINEINSTRUCTIONS
#define _JIT_COMPILER2_MACHINEINSTRUCTIONS

#include "vm/jit/compiler2/DataSegment.hpp"
#include "vm/jit/compiler2/MachineInstruction.hpp"
#include "vm/jit/compiler2/MachineOperandFactory.hpp"
#include "vm/jit/rplpid.hpp"

namespace cacao {
namespace jit {
namespace compiler2 {

class MachineReplacementEntryInst;
class MachineReplacementPointCallSiteInst;
class MachineReplacementPointStaticSpecialInst;
class MachineDeoptInst;

class MachineLabelInst : public MachineInstruction {
public:
	MachineLabelInst() : MachineInstruction("MLabel", &NoOperand, 0) {}
	virtual void emit(CodeMemory* CM) const;
	virtual bool is_label() const {
		return true;
	}
};

class MachinePhiInst : public MachineInstruction {
private:
	PHIInst *phi;
public:
	MachinePhiInst(unsigned num_operands, Type::TypeID type, PHIInst* phi, MachineOperandFactory* MOF)
			: MachineInstruction("MPhi", MOF->CreateVirtualRegister(type),
			  num_operands), phi(phi) {
		for(unsigned i = 0; i < num_operands; ++i) {
			operands[i].op = MOF->CreateUnassignedReg(type);
		}
		get_result().op->set_defining_instruction(this);
	}

	virtual bool is_phi() const {
		return true;
	}
	virtual MachinePhiInst* to_MachinePhiInst() {
		return this;
	}
	// phis are never emitted
	virtual void emit(CodeMemory* CM) const {};

	PHIInst* get_PHIInst() const {
		return phi;
	}
};

/// Simple META instruction that can be used to define value.
/// It does emit nothing.
/// Currently used by TrapInstructions that do not have fixed
/// register.
class MachineDefInst : public MachineInstruction {
public:
	MachineDefInst(Type::TypeID type, MachineOperandFactory* MOF)
		: MachineInstruction("MDef", MOF->CreateVirtualRegister(type), 0) {}
	
	virtual void emit(CodeMemory* CM) const {};
};

#if 0
class MachineConstInst : public MachineInstruction {
public:
	/**
	 * TODO: get const parameter
	 */
	MachineConstInst(s8 value) : MachineInstruction("MConst",
			new Immediate(value), 0) {}
	// mconsts are never emitted
	virtual void emit(CodeMemory* CM) const {};
};

/**
 * Load from memory to register
 */
class MachineLoadInst : public MachineInstruction {
public:
	MachineLoadInst(
			MachineOperand *dst,
			MachineOperand *src)
			: MachineInstruction("MLoad", dst, 1) {
		operands[0].op = src;
	}

};

/**
 * Store register to memory
 */
class MachineStoreInst : public MachineInstruction {
public:
	MachineStoreInst(
			MachineOperand *dst,
			MachineOperand *src)
			: MachineInstruction("MStore", dst, 1) {
		operands[0].op = src;
	}

};

class MachineOperandInst : public MachineInstruction {
private:
	MachineOperand *MO;
public:
	MachineOperandInst(MachineOperand *MO)
			: MachineInstruction("MachineOperandInst", MO, 0), MO(MO) {
	}

};
#endif
class MachineJumpInst : public MachineInstruction {
public:
	MachineJumpInst(const char *name)
		: MachineInstruction(name, &NoOperand, 0) {}
	virtual void emit(CodeMemory* CM) const = 0;
	virtual void link(CodeFragment &CF) const = 0;
};
#if 0
/**
 * Move operand to operand
 */
class MachineMoveInst : public MachineInstruction {
public:
	MachineMoveInst( const char* name,
			MachineOperand *src,
			MachineOperand *dst)
			: MachineInstruction(name, dst, 1) {
		operands[0].op = src;
	}
	virtual bool accepts_immediate(unsigned i, Immediate *imm) const {
		return true;
	}
	virtual MachineMoveInst* to_MachineMoveInst() {
		return this;
	}
	virtual void emit(CodeMemory* CM) const = 0;

};
#endif

/**
 * Represents a point in the program, where it is possible to recover the source
 * state to perform on-stack replacement.
 */
class MachineReplacementPointInst : public MachineInstruction {
	// TODO move this into subclass
private:
	typedef alloc::vector<s4>::type JavalocalIndexMapTy;

	// This could be replaced with a list of (rplpid, op count) pairs.
	SourceStateInst *source_state;

	// first it stores the indices for the source_state and its parents,
	// then those for output operands if this is an entrypoint.
	JavalocalIndexMapTy javalocal_indices;

protected:
	/**
	 * @param source_state the source state for this instruction
	 */
	MachineReplacementPointInst(const char *name, SourceStateInst *source_state)
		: MachineInstruction(name, &NoOperand, source_state != nullptr ? source_state->count_transitive_operands() : 0),
			source_state(source_state),
			javalocal_indices(op_size()) {}

public:
	virtual void emit(CodeMemory* CM) const {};
	virtual void link(CodeFragment &CF) const {};

	virtual MachineReplacementPointInst* to_MachineReplacementPointInst() {
		return this;
	}

	virtual MachineReplacementEntryInst* to_MachineReplacementEntryInst() {
		return NULL;
	}

	virtual MachineReplacementPointCallSiteInst* to_MachineReplacementPointCallSiteInst() {
		return NULL;
	}

	virtual MachineReplacementPointStaticSpecialInst* to_MachineReplacementPointStaticSpecialInst() {
		return NULL;
	}

	virtual MachineDeoptInst* to_MachineDeoptInst() {
		return NULL;
	}

	SourceStateInst *get_source_state() const { return source_state; }

	/**
	 * Set the javalocal index of the i-th operand of this instruction. Pass a
	 * value >= 0 to indicate a valid javalocal index. Pass RPLALLOC_STACK to
	 * indicate that the i-th operand is a stack variable. Pass RPLALLOC_PARAM
	 * to indicate that it is a parameter at a call site.
	 */
	void set_javalocal_index(std::size_t i, s4 javalocal_index) {
		assert(i < op_size());
		javalocal_indices[i] = javalocal_index;
	}

	void push_javalocal_index(s4 javalocal_index) {
		javalocal_indices.push_back(javalocal_index);
	}

	/**
	 * @return the javalocal index of the i-th operand of this instruction. For
	 * possible return values refer to set_javalocal_index().
	 */
	s4 get_javalocal_index(std::size_t i) {
		return javalocal_indices[i];
	}
};

class MachineReplacementEntryInst : public MachineReplacementPointInst {
	rplpid rpid;

public:
	explicit MachineReplacementEntryInst(rplpid rpid)
		: MachineReplacementPointInst("MReplacementEntry", nullptr), rpid(rpid) {}

	virtual void emit(CodeMemory* CM) const {};
	virtual void link(CodeFragment &CF) const {};

	virtual MachineReplacementEntryInst* to_MachineReplacementEntryInst() {
		return this;
	}

	rplpid get_rpid() const {
		return rpid;
	}
};

/**
 * Represents a replacement point at a call site (INVOKE* ICMDs)
 * The reference to the corresponding call MachineInstruction is needed since
 * the replacement point needs to know the size of the callsite (in bytes)
 */
class MachineReplacementPointCallSiteInst : public MachineReplacementPointInst {
private:
	MachineInstruction *call_inst;

public:
	MachineReplacementPointCallSiteInst(MachineInstruction* call_inst, SourceStateInst *source_state)
		: MachineReplacementPointInst("MReplacementPointCallSite", source_state), call_inst(call_inst) {}
	virtual void emit(CodeMemory* CM) const {};
	virtual void link(CodeFragment &CF) const {};

	virtual MachineReplacementPointCallSiteInst* to_MachineReplacementPointCallSiteInst() {
		return this;
	}

	MachineInstruction* get_call_inst() const {
		return call_inst;
	}
};

/**
 * Specialication for INVOKESpecial and INVOKEStatic.
 * Since the target address is known during compilation, this class holds a reference
 * to the DataSegment index where the address is stored so it can be patched
 * during OSR
 */
class MachineReplacementPointStaticSpecialInst : public MachineReplacementPointCallSiteInst {
private:
	/**
	 * DataSegment index that contains the address of the call target
	 */
	DataSegment::IdxTy idx;

public:
	MachineReplacementPointStaticSpecialInst(MachineInstruction* call_inst, SourceStateInst *source_state, DataSegment::IdxTy idx)
		: MachineReplacementPointCallSiteInst(call_inst, source_state), idx(idx) {}
	
	virtual MachineReplacementPointStaticSpecialInst* to_MachineReplacementPointStaticSpecialInst() {
		return this;
	}

	DataSegment::IdxTy get_idx() const {
		return idx;
	}
};

class MachineDeoptInst : public MachineReplacementPointInst {
public:
	MachineDeoptInst(SourceStateInst *source_state)
		: MachineReplacementPointInst("MDeopt", source_state) {}
	virtual void emit(CodeMemory* CM) const {};
	virtual void link(CodeFragment &CF) const {};

	virtual MachineDeoptInst* to_MachineDeoptInst() {
		return this;
	}
};

} // end namespace compiler2
} // end namespace jit
} // end namespace cacao

#endif /* _JIT_COMPILER2_MACHINEINSTRUCTIONS */


/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
