/* src/vm/jit/rplpid.cpp - id of replacement points for on-stack replacement of methods

   Copyright (C) 1996-2013
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/

#include "rplpid.hpp"

#include "toolbox/OStream.hpp"
#include "vm/jit/ir/instruction.hpp"
#include "vm/jit/jit.hpp"
#include "vm/method.hpp"

using namespace cacao;

rplpid::rplpid(methodinfo *method, Position position, instruction* iptr)
	: method(method), position(position), instrid(iptr->flags.bits >> INS_FLAG_ID_SHIFT) {
}

bool rplpid::is_before_bb(basicblock* bb) {
	return position == BEFORE_BB
	       && bb->iinstr
	       && (bb->iinstr->flags.bits >> INS_FLAG_ID_SHIFT) == instrid;
}

namespace cacao {

OStream& operator<<(OStream &OS, const rplpid *id) {
	if (!id) {
		return OS << "(rplpid *) NULL";
	}
	return OS << *id;
}

OStream& operator<<(OStream &OS, const rplpid &id) {
	OS << "(";
	if (id.method) {
		OS << *id.method;
	} else {
		OS << "(methodinfo *) NULL";
	}
	static char const*position_names[] =  {"INVALID ", "BEFORE_BB ", "BEFORE_CALL ", "AFTER "};
	return OS << position_names[id.position] << id.instrid << ")";
}

} // end namespace cacao

/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
