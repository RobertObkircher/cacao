/** tests/compiler2/junit/Idiv.java
*
* Copyright (C) 1996-2014
* CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO
*
* This file is part of CACAO.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301, USA.
*
*/
package org.cacaojvm.compiler2.test;

import org.junit.Test;

public class Idiv extends Compiler2TestBase {

	@Test
	public void test0() {
		testResultEqual("idiv", "(II)I", 10, 5);
	}

	@Test
	public void test1() {
		testResultEqual("idiv", "(II)I", 11, 5);
	}

	@Test
	public void test2() {
		testResultEqual("idiv", "(II)I", 5, 5);
	}

	@Test
	public void test3() {
		testResultEqual("idiv", "(II)I", 3, 5);
	}
	
	@Test
	public void test4() {
		testResultEqual("idiv", "(II)I", -10, 5);
	}

	@Test
	public void test5() {
		testResultEqual("idiv", "(II)I", 10, -5);
	}

	@Test
	public void test6() {
		testResultEqual("idiv", "(II)I", -10, -5);
	}

	@Test
	public void test7() {
		testResultEqual("idiv", "(II)I", 0, 5);
	}

	@Test
	public void test8() {
		testResultEqual("idiv", "(II)I", 5, 0);
	}

	/**
	 * This is the method under test.
	 */
	static int idiv(int x, int y) {
		return x / y;
	}

	@Test
	public void testIdivpow2() {
		for (int x : new int[] { -100, -33, -32, -31, -3, -2, -1, 0, 1, 2, 3, 10, 31, 32, 33, 100 }) {
			testResultEqual("idiv2", "(I)I", x);
			testResultEqual("idiv32", "(I)I", x);
		}
	}

	static int idiv2(int x) {
		return x / 2;
	}

	static int idiv32(int x) {
		return x / 32;
	}

	@Test
	public void testConstDivBy0() {
		testResultEqual("constDivBy0", "()I");
	}
	static int constDivBy0() {
		return 1 / 0; // compiler2 crashed in constant propagation
	}

}

