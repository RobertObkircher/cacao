/** tests/compiler2/junit/DeoptimizeInlined.java
*
* Copyright (C) 1996-2022
* CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO
*
* This file is part of CACAO.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301, USA.
*
*/
package org.cacaojvm.compiler2.test;

import org.junit.Test;

public class DeoptimizeInlined extends Compiler2TestBase {

	@Test
	public void test1() {
		testResultEqual("caller", "(II)I", 1, 2); // 1 ^ 2 ^ 4 ^ 8 = 15
	}

	static int caller(int a, int b) {
		return a ^ inlined(a, b) ^ b;
	}

	static int inlined(int a, int b) {
		int c = a * 4;
		Compiler2Test.deoptimizeHere();
		int d = b * 4;
		return c ^ d;
	}

	@Test
	public void test2() {
		testResultEqual("fib", "(I)I", 5);
	}

	static int fib(int n) {
		if(n == 0) {
			Compiler2Test.deoptimizeHere();
			return 0;
		}
		return n + fib(n - 1);
	}

	@Test
	public void test3() {
		testResultEqual("operands", "(IJFD)D", 5, 8L, 3.1f, 2.2d);
	}

	static double operands(int i, long j, float f, double d) {
		// before call to deopt:
		// locals: 0:I, 1:L, 3:F, 4:D
		// stack: I, L, F, D
		// param: F, D
		return d + (f * (j + i * deopt(3.14f, 1000.0) * 11));
	}

	static int deopt(float x, double y) {
		// locals: 0:F, 1:D
		Compiler2Test.deoptimizeHere();
		return (int) (x + y);
	}

	@Test
	public void test4() {
		testResultEqual("floats", "(JFD)D", 1L, 3.1f, 2.2d);
	}

	static double floats(long j, float f, double d) {
		return j + d * deopt(f, d);
	}

}