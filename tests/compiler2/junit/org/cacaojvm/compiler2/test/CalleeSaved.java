/** tests/compiler2/junit/CalleeSaved.java - CalleeSaved
 *
 * Copyright (C) 2024
 * CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO
 *
 * This file is part of CACAO.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */
package org.cacaojvm.compiler2.test;

import org.junit.Test;

public class CalleeSaved extends Compiler2TestBase {

	@Test
	public void testBaselineLeaf() {
		compileBaseline(CalleeSaved.class, "leafMethod", "(I)I");
		testResultEqual("leafCallerMethod", "(II)I", (int) 0, (int) 10);
	}

	@Test
	public void testCompiler2Leaf() {
		compileCompiler2(CalleeSaved.class, "leafMethod", "(I)I");
		testResultEqual("leafCallerMethod", "(II)I", (int) 0, (int) 10);
	}

	@Test
	public void testBaselineNonLeaf() {
		compileBaseline(CalleeSaved.class, "nonLeafMethod", "(I)I");
		testResultEqual("nonLeafCallerMethod", "(II)I", (int) 0, (int) 10);
	}

	@Test
	public void testCompiler2NonLeaf() {
		compileCompiler2(CalleeSaved.class, "nonLeafMethod", "(I)I");
		testResultEqual("nonLeafCallerMethod", "(II)I", (int) 0, (int) 10);
	}

	@Test
	public void testLeafDeoptimize() {
		compileCompiler2(CalleeSaved.class, "leafMethod", "(I)I");
		testResultEqual("leafCallerMethod", "(II)I", (int) 0, (int) 11);
	}

	@Test
	public void testNonLeafDeoptimize() {
		compileCompiler2(CalleeSaved.class, "nonLeafMethod", "(I)I");
		testResultEqual("nonLeafCallerMethod", "(II)I", (int) 0, (int) 11);
	}

	@Test
	public void testOsrLeaf() {
		compileBaseline(CalleeSaved.class, "leafMethod", "(I)I");
		testResultEqual("leafCallerMethod", "(II)I", (int) 0, (int) 200000);
	}

	@Test
	public void testOsrNonLeaf() {
		compileBaseline(CalleeSaved.class, "nonLeafMethod", "(I)I");
		testResultEqual("nonLeafCallerMethod", "(II)I", (int) 0, (int) 200000);
	}

	@Test
	public void testOsrLeafDeoptimize() {
		compileBaseline(CalleeSaved.class, "leafMethod", "(I)I");
		testResultEqual("leafCallerMethod", "(II)I", (int) 0, (int) 200001);
	}

	@Test
	public void testOsrNonLeafDeoptimize() {
		compileBaseline(CalleeSaved.class, "nonLeafMethod", "(I)I");
		testResultEqual("nonLeafCallerMethod", "(II)I", (int) 0, (int) 200001);
	}

	public static int leafCallerMethod(int i, int iterations) {
		// hopefully use a lot of registers, use division to avoid re-ordering
		int a1  = 1000 / ( 1 + i);
		int a2  = 1000 / ( 2 + i);
		int a3  = 1000 / ( 3 + i);
		int a4  = 1000 / ( 4 + i);
		int a5  = 1000 / ( 5 + i);
		int a6  = 1000 / ( 6 + i);
		int a7  = 1000 / ( 7 + i);
		int a8  = 1000 / ( 8 + i);
		int a9  = 1000 / ( 9 + i);
		int a10 = 1000 / (10 + i);
		int a11 = 1000 / (11 + i);
		int a12 = 1000 / (12 + i);
		int a13 = 1000 / (13 + i);
		int a14 = 1000 / (14 + i);
		int a15 = 1000 / (15 + i);
		int a16 = 1000 / (16 + i);
		int a17 = 1000 / (17 + i);
		int a18 = 1000 / (18 + i);
		int a19 = 1000 / (19 + i);
		int a20 = 1000 / (20 + i);

		int result = leafMethod(iterations);
		//System.out.println("result=" + result + ", i=" + i + ", as: " + a1 + " " + a2 + " " + a3 + " " + a4 + " " + a5 + " " + a6 + " " + a7 + " " + a8 + " " + a9 + " " + a10 + " " + a11 + " " + a12 + " " + a13 + " " + a14 + " " + a15 + " " + a16 + " " + a17 + " " + a18 + " " + a19 + " " + a20 );

		return   ((100000000+result)/a1)
		       + ((100000000+result)/a2)
		       + ((100000000+result)/a3)
		       + ((100000000+result)/a4)
		       + ((100000000+result)/a5)
		       + ((100000000+result)/a6)
		       + ((100000000+result)/a7)
		       + ((100000000+result)/a8)
		       + ((100000000+result)/a9)
		       + ((100000000+result)/a10)
		       + ((100000000+result)/a11)
		       + ((100000000+result)/a12)
		       + ((100000000+result)/a13)
		       + ((100000000+result)/a14)
		       + ((100000000+result)/a15)
		       + ((100000000+result)/a16)
		       + ((100000000+result)/a17)
		       + ((100000000+result)/a18)
		       + ((100000000+result)/a19)
		       + ((100000000+result)/a20);
	}

	private static final RuntimeException throwMe = new RuntimeException("thrown from leafMethod");

	public static int leafMethod(int iterations) {
		// use a lot of registers
		int a1 = 1;
		int a2 = 2;
		int a3 = 3;
		int a4 = 4;
		int a5 = 5;
		int a6 = 6;
		int a7 = 7;
		int a8 = 8;
		int a9 = 9;
		int a10 = 10;
		int a11 = 11;
		int a12 = 12;
		// TODO add more variables once when this error is fixed: RegisterAssignmentPass: Fallback Graph-coloring for definitions not implemented!

		for (int i = 0; i < iterations; ++i) {
			a1 = (a1 % 2) == 0 ? a1 / 2 : 3 * a1 + 1;
			a2 = (a2 % 2) == 0 ? a2 / 2 : 3  * a2 + 1;
			a3 = (a3 % 2) == 0 ? a3 / 2 : 3  * a3 + 1;
			a4 = (a4 % 2) == 0 ? a4 / 2 : 3  * a4 + 1;
			a5 = (a5 % 2) == 0 ? a5 / 2 : 3  * a5 + 1;
			a6 = (a6 % 2) == 0 ? a6 / 2 : 3  * a6 + 1;
			a7 = (a7 % 2) == 0 ? a7 / 2 : 3  * a7 + 1;
			a8 = (a8 % 2) == 0 ? a8 / 2 : 3  * a8 + 1;
			a9 = (a9 % 2) == 0 ? a9 / 2 : 3  * a9 + 1;
			a10 = (a10 % 2) == 0 ? a10 / 2 : 3  * a10 + 1;
			a11 = (a11 % 2) == 0 ? a11 / 2 : 3  * a11 + 1;
			a12 = (a12 % 2) == 0 ? a12 / 2 : 3  * a12 + 1;

			if (i == iterations - 2) {
				if (iterations == 200001 || iterations == 11) {
					throw throwMe; // TODO find a better way than the unimplemented opcode to guarantee that deoptimization occurs here
				}
			}
		}

		return a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10 + a11 + a12;
	}

	public static int nonLeafCallerMethod(int i, int iterations) {
		// hopefully use a lot of registers, use division to avoid re-ordering
		int a1  = 1000 / ( 1 + i);
		int a2  = 1000 / ( 2 + i);
		int a3  = 1000 / ( 3 + i);
		int a4  = 1000 / ( 4 + i);
		int a5  = 1000 / ( 5 + i);
		int a6  = 1000 / ( 6 + i);
		int a7  = 1000 / ( 7 + i);
		int a8  = 1000 / ( 8 + i);
		int a9  = 1000 / ( 9 + i);
		int a10 = 1000 / (10 + i);
		int a11 = 1000 / (11 + i);
		int a12 = 1000 / (12 + i);
		int a13 = 1000 / (13 + i);
		int a14 = 1000 / (14 + i);
		int a15 = 1000 / (15 + i);
		int a16 = 1000 / (16 + i);
		int a17 = 1000 / (17 + i);
		int a18 = 1000 / (18 + i);
		int a19 = 1000 / (19 + i);
		int a20 = 1000 / (20 + i);

		int result = nonLeafMethod(iterations);
		//System.out.println("result=" + result + ", i=" + i + ", as: " + a1 + " " + a2 + " " + a3 + " " + a4 + " " + a5 + " " + a6 + " " + a7 + " " + a8 + " " + a9 + " " + a10 + " " + a11 + " " + a12 + " " + a13 + " " + a14 + " " + a15 + " " + a16 + " " + a17 + " " + a18 + " " + a19 + " " + a20 );

		return   ((100000000+result)/a1)
		       + ((100000000+result)/a2)
		       + ((100000000+result)/a3)
		       + ((100000000+result)/a4)
		       + ((100000000+result)/a5)
		       + ((100000000+result)/a6)
		       + ((100000000+result)/a7)
		       + ((100000000+result)/a8)
		       + ((100000000+result)/a9)
		       + ((100000000+result)/a10)
		       + ((100000000+result)/a11)
		       + ((100000000+result)/a12)
		       + ((100000000+result)/a13)
		       + ((100000000+result)/a14)
		       + ((100000000+result)/a15)
		       + ((100000000+result)/a16)
		       + ((100000000+result)/a17)
		       + ((100000000+result)/a18)
		       + ((100000000+result)/a19)
		       + ((100000000+result)/a20);
	}

	public static int nonLeafMethod(int iterations) {
		// use a lot of registers
		int a1 = 1;
		int a2 = 2;
		int a3 = 3;
		int a4 = 4;
		int a5 = 5;
		int a6 = 6;
		int a7 = 7;
		int a8 = 8;
		int a9 = 9;
		int a10 = 10;
		int a11 = 11;
		int a12 = 12;
		// TODO add more variables once when this error is fixed: RegisterAssignmentPass: Fallback Graph-coloring for definitions not implemented!

		for (int i = 0; i < iterations; ++i) {
			a1 = (a1 % 2) == 0 ? a1 / 2 : 3 * a1 + 1;
			a2 = (a2 % 2) == 0 ? a2 / 2 : 3  * a2 + 1;
			a3 = (a3 % 2) == 0 ? a3 / 2 : 3  * a3 + 1;
			a4 = (a4 % 2) == 0 ? a4 / 2 : 3  * a4 + 1;
			a5 = (a5 % 2) == 0 ? a5 / 2 : 3  * a5 + 1;
			a6 = (a6 % 2) == 0 ? a6 / 2 : 3  * a6 + 1;
			a7 = (a7 % 2) == 0 ? a7 / 2 : 3  * a7 + 1;
			a8 = (a8 % 2) == 0 ? a8 / 2 : 3  * a8 + 1;
			a9 = (a9 % 2) == 0 ? a9 / 2 : 3  * a9 + 1;
			a10 = (a10 % 2) == 0 ? a10 / 2 : 3  * a10 + 1;
			a11 = (a11 % 2) == 0 ? a11 / 2 : 3  * a11 + 1;
			a12 = (a12 % 2) == 0 ? a12 / 2 : 3  * a12 + 1;

			if (i == iterations - 2) {
				if (iterations == 200001 || iterations == 11) {
					Compiler2Test.deoptimizeHere();
				}
			}
		}

		return a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10 + a11 + a12;
	}

	@Test
	public void testSpillingProblem() {
		testResultEqual("testSpillingProblemInner", "(I)I", (int) 200000);
	}

	public static int testSpillingProblemInner(int iterations) {
		int a1 = 1;
		for (int i = 0; i < iterations; ++i) {
			// in combination with OSR this caused reloads to be inserted before the cmp instruction,
			// which made it compare the wrong value.
			a1 = (a1 % 100) == 0 ? 1 : 100;
		}
		return a1;
	}

}
