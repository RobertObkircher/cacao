/* src/vm/jit/rplpid.hpp - id of replacement points for on-stack replacement of methods

   Copyright (C) 1996-2022
   CACAOVM - Verein zur Foerderung der freien virtuellen Maschine CACAO

   This file is part of CACAO.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301, USA.

*/

#ifndef RPLPID_HPP_
#define RPLPID_HPP_ 1

#include "config.h"                     // for ENABLE_REPLACEMENT, etc
#include "vm/types.hpp"                 // for s4, u1, ptrint, u4, s8

struct instruction;
struct methodinfo;

#if defined(ENABLE_REPLACEMENT)

/**
 * Replacement points are placed right BEFORE the first instruction in a basic block, before call
 * sites and AFTER instructions with side effects.
 *
 * Previously the rplpoint was always identified by the id of the following instruction i.e. the
 * first instruction that could use it. I changed it because this is more similar to how compiler2
 * handles SourceStates and I wasn't entirely sure if the last instruction in a basic block could
 * have side effects. This also fixed a deoptimization bug where we looked up the the replacement
 * point for the basic block when we should have looked up the replacement point for the call site
 * to build the stack frame.
 *
 * The method is required because of inlining.
 */
struct rplpid {
	enum Position {
		INVALID,
		BEFORE_BB,
		BEFORE_CALL,
		AFTER,
	};

	methodinfo *method;
	Position position;
	u4 instrid;

	/**
	 * Construct a dummy value for the default constructor of sourceframe_t
	 */
	rplpid() = default;

	rplpid(methodinfo *, Position, instruction*);

	bool operator==(const rplpid& rhs) const
	{
		return method == rhs.method && instrid == rhs.instrid && position == rhs.position;
	}

	bool valid() const
	{
		return position != INVALID;
	}

	bool is_before_bb(struct basicblock *bb);
};

namespace cacao {

class OStream;

OStream& operator<<(OStream &OS, const rplpid *rp);
OStream& operator<<(OStream &OS, const rplpid &rp);

} // end namespace cacao

#endif // ENABLE_REPLACEMENT

#endif // RPLPID_HPP_


/*
 * These are local overrides for various environment variables in Emacs.
 * Please do not remove this and leave it at the end of the file, where
 * Emacs will automagically detect them.
 * ---------------------------------------------------------------------
 * Local variables:
 * mode: c++
 * indent-tabs-mode: t
 * c-basic-offset: 4
 * tab-width: 4
 * End:
 * vim:noexpandtab:sw=4:ts=4:
 */
